﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SPOT_export_gui
{
    public partial class SpotExport : Form
    {
        string picturePath;
        public SpotExport()
        {
            InitializeComponent();

            //imageListView.ThumbnailCaching += new Manina.Windows.Forms.ThumbnailCachingEventHandler(imageListView1_ThumbnailCaching);
            //imageListView.ThumbnailCached += new Manina.Windows.Forms.ThumbnailCachedEventHandler(imageListView1_ThumbnailCached);
            //imageListView.CacheError += new Manina.Windows.Forms.CacheErrorEventHandler(imageListView1_CacheError);
            //imageListView.ItemCollectionChanged += new ItemCollectionChangedEventHandler(imageListView_ItemCollectionChanged);
            //imageListView.KeyPress += new KeyPressEventHandler(imageListView_KeyPress);
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (ChooseSourcePath.ShowDialog() == DialogResult.OK)
            {
                picturePath = ChooseSourcePath.SelectedPath;
                dirPathTextBox.Text = picturePath;
                LoadPictures();
            }
        }

        private void dirPathTextBox_Leave(object sender, EventArgs e)
        {
            picturePath = dirPathTextBox.Text;
            LoadPictures();
        }

        private void LoadPictures()
        {
            string[] files;
            try
            {
                imageListView.Items.Clear();
                files = Directory.GetFiles(picturePath, "*.jpg");

                if (files.Length == 0)
                {
                    MessageBox.Show("A megadott mappában nem található jpg fájl", "SPOT export error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                imageListView.SuspendLayout();
                for (int i = 0; i < files.Length; i++)
                {
                    imageListView.Items.Add(files[i]);
                }
                imageListView.ResumeLayout();
            }
            catch (DirectoryNotFoundException e)
            {

            }
            catch (ArgumentException e)
            {

            }
        }

        private void addcoverButton_Click(object sender, EventArgs e)
        {
            if (imageListView.SelectedItems.Count > 0)
            {
                coverTextBox.Text = imageListView.SelectedItems[0].Text;
            }
        }

        private void addphotographerButton_Click(object sender, EventArgs e)
        {
            if (photographerListBox.SelectedItems.Count > 0)
            {
                if (photographerTextBox.Text == "")
                {
                    photographerTextBox.Text += "\"" + photographerListBox.SelectedItem.ToString() + "\"";
                }
                else
                {
                    photographerTextBox.Text += ", \"" + photographerListBox.SelectedItem.ToString() + "\"";
                }
            }
        }

        private void addeventButton_Click(object sender, EventArgs e)
        {
            if (eventsListBox.SelectedItems.Count > 0)
            {
                if (eventTextBox.Text == "")
                {
                    eventTextBox.Text += "\"" + eventsListBox.SelectedItem.ToString() + "\"";
                }
                else
                {
                    eventTextBox.Text += ", \"" + eventsListBox.SelectedItem.ToString() + "\"";
                }
            }
        }

        private void SpotExport_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1)
            {
                dirPathTextBox.Text = Path.GetDirectoryName(args[1]);
                picturePath = dirPathTextBox.Text;
                LoadPictures();
            }

            LoadLists();
        }

        private void LoadLists()
        {            
            try
            {
                string currentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                string archiveFolder = Path.Combine(currentDirectory, "data");
                string[] lines = System.IO.File.ReadAllLines(archiveFolder + @"\photographers.txt");

                photographerListBox.Items.Clear();

                List<string> photographerList = new List<string>();

                eventsListBox.Items.Clear();

                foreach (string line in lines)
                {
                    photographerList.Add(line);
                }

                List<string> photographerEventList = photographerList.OrderBy(o => o).ToList();

                foreach (string line in photographerEventList)
                {
                    photographerListBox.Items.Add(line);
                }
            }
            catch (IOException ex)
            { }

            try
            {
                string currentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                string archiveFolder = Path.Combine(currentDirectory, "data");
                string[] lines = System.IO.File.ReadAllLines(archiveFolder + @"\events.txt");
                List<string> eventList = new List<string>();

                eventsListBox.Items.Clear();

                foreach (string line in lines)
                {
                    eventList.Add(line);
                }

                List<string> sortedEventList = eventList.OrderBy(o => o).ToList();

                foreach (string line in sortedEventList)
                {
                    eventsListBox.Items.Add(line);
                }
            }
            catch (IOException ex)
            { }
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            if((titleTextBox.Text == "") || (coverTextBox.Text == "") || (photographerTextBox.Text == "") || (eventTextBox.Text == ""))
            {
                MessageBox.Show("Nincs kitöltve minden mező!", "SPOT export error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string[] content = {
                "{",
                "  \"title\": \""+titleTextBox.Text+"\",",
                "  \"cover\": \"400/"+coverTextBox.Text+"\",",
                "  \"photographers\": ["+photographerTextBox.Text+"],",
                "  \"events\": ["+eventTextBox.Text+"]",
                "}"
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = saveFileDialog.FileName;
                System.IO.File.WriteAllLines(path, content);
            }
        }

        private void dirPathTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                picturePath = dirPathTextBox.Text;
                LoadPictures();
            }
        }
    }
}
