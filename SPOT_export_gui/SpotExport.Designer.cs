﻿namespace SPOT_export_gui
{
    partial class SpotExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageListView = new Manina.Windows.Forms.ImageListView();
            this.dirPathTextBox = new System.Windows.Forms.TextBox();
            this.openButton = new System.Windows.Forms.Button();
            this.ChooseSourcePath = new System.Windows.Forms.FolderBrowserDialog();
            this.photographerListBox = new System.Windows.Forms.ListBox();
            this.coverTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.photographerTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.eventTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.eventsListBox = new System.Windows.Forms.ListBox();
            this.addcoverButton = new System.Windows.Forms.Button();
            this.addphotographerButton = new System.Windows.Forms.Button();
            this.addeventButton = new System.Windows.Forms.Button();
            this.generateButton = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(415, 41);
            this.titleTextBox.MaxLength = 50;
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(259, 20);
            this.titleTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(412, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Album címe";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageListView
            // 
            this.imageListView.ColumnHeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.imageListView.GroupHeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.imageListView.Location = new System.Drawing.Point(13, 41);
            this.imageListView.MultiSelect = false;
            this.imageListView.Name = "imageListView";
            this.imageListView.PersistentCacheDirectory = "";
            this.imageListView.PersistentCacheSize = ((long)(100));
            this.imageListView.Size = new System.Drawing.Size(367, 348);
            this.imageListView.TabIndex = 2;
            // 
            // dirPathTextBox
            // 
            this.dirPathTextBox.Location = new System.Drawing.Point(13, 13);
            this.dirPathTextBox.Name = "dirPathTextBox";
            this.dirPathTextBox.Size = new System.Drawing.Size(317, 20);
            this.dirPathTextBox.TabIndex = 3;
            this.dirPathTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dirPathTextBox_KeyUp);
            this.dirPathTextBox.Leave += new System.EventHandler(this.dirPathTextBox_Leave);
            // 
            // openButton
            // 
            this.openButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.openButton.Location = new System.Drawing.Point(336, 12);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(44, 21);
            this.openButton.TabIndex = 4;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // ChooseSourcePath
            // 
            this.ChooseSourcePath.Description = "\'web\' mappa kiválasztása...";
            this.ChooseSourcePath.ShowNewFolderButton = false;
            // 
            // photographerListBox
            // 
            this.photographerListBox.FormattingEnabled = true;
            this.photographerListBox.Location = new System.Drawing.Point(709, 35);
            this.photographerListBox.Name = "photographerListBox";
            this.photographerListBox.Size = new System.Drawing.Size(201, 108);
            this.photographerListBox.TabIndex = 5;
            // 
            // coverTextBox
            // 
            this.coverTextBox.Location = new System.Drawing.Point(415, 80);
            this.coverTextBox.MaxLength = 30;
            this.coverTextBox.Name = "coverTextBox";
            this.coverTextBox.Size = new System.Drawing.Size(259, 20);
            this.coverTextBox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(412, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Borítókép";
            // 
            // photographerTextBox
            // 
            this.photographerTextBox.Location = new System.Drawing.Point(415, 119);
            this.photographerTextBox.MaxLength = 130;
            this.photographerTextBox.Name = "photographerTextBox";
            this.photographerTextBox.Size = new System.Drawing.Size(259, 20);
            this.photographerTextBox.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(412, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Fotós(ok)";
            // 
            // eventTextBox
            // 
            this.eventTextBox.Location = new System.Drawing.Point(415, 158);
            this.eventTextBox.MaxLength = 130;
            this.eventTextBox.Name = "eventTextBox";
            this.eventTextBox.Size = new System.Drawing.Size(259, 20);
            this.eventTextBox.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(412, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Esemény(ek)";
            // 
            // eventsListBox
            // 
            this.eventsListBox.FormattingEnabled = true;
            this.eventsListBox.Location = new System.Drawing.Point(709, 156);
            this.eventsListBox.Name = "eventsListBox";
            this.eventsListBox.Size = new System.Drawing.Size(201, 108);
            this.eventsListBox.TabIndex = 5;
            // 
            // addcoverButton
            // 
            this.addcoverButton.Location = new System.Drawing.Point(386, 78);
            this.addcoverButton.Name = "addcoverButton";
            this.addcoverButton.Size = new System.Drawing.Size(23, 23);
            this.addcoverButton.TabIndex = 6;
            this.addcoverButton.Text = ">";
            this.addcoverButton.UseVisualStyleBackColor = true;
            this.addcoverButton.Click += new System.EventHandler(this.addcoverButton_Click);
            // 
            // addphotographerButton
            // 
            this.addphotographerButton.Location = new System.Drawing.Point(680, 117);
            this.addphotographerButton.Name = "addphotographerButton";
            this.addphotographerButton.Size = new System.Drawing.Size(23, 23);
            this.addphotographerButton.TabIndex = 7;
            this.addphotographerButton.Text = "<";
            this.addphotographerButton.UseVisualStyleBackColor = true;
            this.addphotographerButton.Click += new System.EventHandler(this.addphotographerButton_Click);
            // 
            // addeventButton
            // 
            this.addeventButton.Location = new System.Drawing.Point(680, 156);
            this.addeventButton.Name = "addeventButton";
            this.addeventButton.Size = new System.Drawing.Size(23, 23);
            this.addeventButton.TabIndex = 7;
            this.addeventButton.Text = "<";
            this.addeventButton.UseVisualStyleBackColor = true;
            this.addeventButton.Click += new System.EventHandler(this.addeventButton_Click);
            // 
            // generateButton
            // 
            this.generateButton.Location = new System.Drawing.Point(473, 184);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(134, 23);
            this.generateButton.TabIndex = 8;
            this.generateButton.Text = "INFO fájl generálás";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "info.txt";
            this.saveFileDialog.Filter = "Text File|*.txt";
            // 
            // SpotExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 401);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.addeventButton);
            this.Controls.Add(this.addphotographerButton);
            this.Controls.Add(this.addcoverButton);
            this.Controls.Add(this.eventsListBox);
            this.Controls.Add(this.photographerListBox);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.dirPathTextBox);
            this.Controls.Add(this.imageListView);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eventTextBox);
            this.Controls.Add(this.photographerTextBox);
            this.Controls.Add(this.coverTextBox);
            this.Controls.Add(this.titleTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SpotExport";
            this.Text = "SPOT export";
            this.Load += new System.EventHandler(this.SpotExport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ImageList imageList1;
        private Manina.Windows.Forms.ImageListView imageListView;
        private System.Windows.Forms.TextBox dirPathTextBox;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.FolderBrowserDialog ChooseSourcePath;
        private System.Windows.Forms.ListBox photographerListBox;
        private System.Windows.Forms.TextBox coverTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox photographerTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox eventTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox eventsListBox;
        private System.Windows.Forms.Button addcoverButton;
        private System.Windows.Forms.Button addphotographerButton;
        private System.Windows.Forms.Button addeventButton;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

